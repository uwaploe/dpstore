package main

import (
	"os"

	"golang.org/x/exp/slog"
)

type dbugLevel struct{}

func (d dbugLevel) Level() slog.Level {
	return slog.LevelDebug
}

func initLogger(debug bool) *slog.Logger {
	opts := slog.HandlerOptions{}
	if debug {
		opts.Level = dbugLevel{}
	}
	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		opts.ReplaceAttr = func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				return slog.Attr{}
			}
			return a
		}
	}

	return slog.New(slog.NewTextHandler(os.Stderr, &opts))
}

func abort(msg string, err error, args ...any) {
	newargs := append([]any{slog.Any("err", err)}, args...)

	slog.Error(msg, newargs...)
	os.Exit(1)
}
