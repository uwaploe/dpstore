# Deep Profiler Database Uploader

The `dpstore` program is used to upload a directory of Deep Profiler data
files (representing a single profile) to
an [InfluxDB](https://www.influxdata.com/time-series-platform/influxdb/)
database. The contents of each sensor data file are stored in a separate
measurement, the measurement name matches the sensor ID. The exception to
this rule is the *evlogger* file which is stored in a measurement named
*events*.

## Usage

``` shellsession
$ dpstore --help

Usage: dpstore [options] DATADIR DBURL DBNAME

Read a series of DP data files from DATADIR and write the data
records to an InfluxDB database DBNAME at DBURL
  -batch int
    	Batch size for database submission (default 1000)
  -pword string
    	Database password
  -tags string
    	Tags to add to every entry
  -user string
    	Database user name
  -version
    	Show program version information and exit
```

The `extras` directory contains a Bash shell script which can be used to
upload an entire archive of Deep Profiler data.
