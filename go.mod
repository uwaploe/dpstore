module bitbucket.org/uwaploe/dpstore

go 1.18

require (
	bitbucket.org/uwaploe/go-dputil v1.3.0
	github.com/google/go-cmp v0.5.9
	github.com/influxdata/influxdb1-client v0.0.0-20190809212627-fc22c7df067e
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	golang.org/x/crypto v0.1.0
	golang.org/x/exp v0.0.0-20230807204917-050eac23e9de
)

require (
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	golang.org/x/net v0.1.0 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/term v0.1.0 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
