package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func profilerData(oldfmt bool) map[string]interface{} {
	m := map[string]interface{}{
		"current":       -40,
		"energy":        1068000,
		"humidity":      123,
		"itemp":         []interface{}{104, 0},
		"motor_current": 0,
		"pressure":      577250,
		"profile":       uint64(9602),
		"rel_charge":    89,
		"state":         "stationary",
		"vmon":          14721,
		"voltage":       15372,
	}

	if !oldfmt {
		m["ipr"] = 145
		m["speed"] = 250
	}
	return m
}

func profilerCvt(oldfmt bool) map[string]interface{} {
	m := map[string]interface{}{
		"current":       float64(-0.04),
		"energy":        float64(1068.0),
		"humidity":      float64(12.3),
		"itemp_0":       float64(10.4),
		"itemp_1":       float64(0),
		"motor_current": float64(0),
		"pressure":      float64(577.25),
		"profile":       int64(9602),
		"rel_charge":    float64(89),
		"state":         "stationary",
		"vmon":          float64(14.721),
		"voltage":       float64(15.372),
	}

	if !oldfmt {
		m["ipr"] = float64(14.5)
		m["speed"] = float64(.25)
	}
	return m
}

func testFilter(t *testing.T, f Filter, in, out map[string]interface{}) {
	m := f(in)
	if diff := cmp.Diff(out, m); diff != "" {
		t.Errorf("testFilter() mismatch (-want +got):\n%s", diff)
	}
}

func TestNullFilter(t *testing.T) {
	in := profilerData(false)
	testFilter(t, NullFilter(), in, in)
}

func TestProfilerFilter(t *testing.T) {
	testFilter(t, ProfilerFilter(scaleValues), profilerData(true), profilerCvt(true))
	testFilter(t, ProfilerFilter(scaleValues), profilerData(false), profilerCvt(false))
}

//  LocalWords:  ipr
