#!/usr/bin/env bash
#
# Use inotify to watch a directory for compressed TAR archives
# of Deep Profiler data. When a new archive arrives, unpack it
# and upload the contents to an InfluxDB database with dpstore.
#
: ${DBURL=http://10.95.97.213:8086}
: ${DBNAME=dp_2017}
: ${DPCID=7}
: ${DPSITE=tank}

[[ -f ./.dpdb ]] && source ./.dpdb

credentials=()
if [[ -f $HOME/.dpdbcreds ]]; then
    user=
    pword=
    IFS=: read user pword < $HOME/.dpdbcreds
    credentials=(--user "$user:$pword")
fi

watchdir="${1:-$PWD}"
workdir="${TMPDIR:-/var/tmp}"

echo "Watching $watchdir"
while read path action file; do
    if [[ $file = *.tgz ]]; then
        name="${file%.*}"
        pnum="${name##_*}"
        tar -C "$workdir" -x -z -f "${path}/${file}" && {
            echo "Uploading profile $pnum ..."
            dpstore \
                --tags pnum=$pnum,site=$DPSITE,dpcid=$DPCID \
                "${credentials[@]}" \
                "${workdir}/${name}" "$DBURL" "$DBNAME"
            rm -rf "${workdir}/${name}"
        }
    fi
done < <(inotifywait -m -e moved_to -e close_write "$watchdir")
