#!/usr/bin/env bash
shopt -s extglob
#
# Upload the contents of a DP data archive to an InfluxDB database
#
: ${DBURL=http://10.95.97.213:8086}
: ${DBNAME=dp_2017}
: ${DPCID=7}
: ${DPSITE=tank}

getplist ()
{
    for d in dataset_*; do
        case "$d" in
            *.tgz)
                d="${d##*_}"
                echo "${d%.*}"
            ;;
            *)
                echo "${d##*_}"
                ;;
        esac
    done | uniq
}

[[ -f ./.dpdb ]] && source ./.dpdb

credentials=()
if [[ -f $HOME/.dpdbcreds ]]; then
    user=
    pword=
    IFS=: read user pword < $HOME/.dpdbcreds
    credentials=(--user "${user}:${pword}")
fi

# Build a list of profile numbers
profiles=()
while read; do
    profiles+=("$REPLY")
done < <(getplist)
# The default directory list
dirlist=(dataset_*)

# The user can optionally specify the starting and ending profile
# numbers on the command line.
if [[ $# -gt 0 ]]; then
    start="$1"
    if [[ $# -gt 1 ]]; then
        end="$2"
    else
        # If the ending number is not given, default to the last
        # available profile.
        n="${#profiles[@]}"
        end="${profiles[n-1]}"
    fi

    dirlist=()
    for i in $(seq $start $end); do
        dirlist+=(dataset_*_${i}*(.tgz))
    done
fi

for d in "${dirlist[@]}"; do
    if [[ $d = *.tgz ]]; then
        echo -n "Expanding $d ... "
        tar xzf "$d"
        echo "done"
        zapdir="yes"
        d="${d%.*}"
    else
        zapdir="no"
    fi
    pnum="${d##*_}"
    echo "Uploading profile $pnum ..."
    dpstore \
        --tags pnum=$pnum,site=$DPSITE,dpcid=$DPCID \
        "${credentials[@]}" \
        "$d" "$DBURL" "$DBNAME"
    if [[ $zapdir = "yes" ]]; then
        echo -n "Removing $d ... "
        rm -rf "$d"
        echo "done"
    fi
done
