package main

import (
	"errors"
	"fmt"
	"io"
	"time"

	"bitbucket.org/uwaploe/go-dputil"
	"github.com/vmihailenco/msgpack"
)

// DataSource provides data records for an InfluxDB database
type DataSource interface {
	// Name returns the measurement name
	Name() string
	// Tags returns a map of tags and values
	Tags() map[string]string
	// Next advances to the next data record, returning
	// false at the end of the stream
	Next() bool
	// Err returns the most recent error
	Err() error
	// Measurement returns the current timestamp and data record
	Measurement() (time.Time, map[string]interface{})
	// Precision returns the timestamp precision
	Precision() string
}

// DpSource reads a Deep Profiler data archive and implements the
// DataSource interface.
type DpSource struct {
	rdr  io.Reader
	err  error
	vals []interface{}
	t    time.Time
	rec  map[string]interface{}
	dec  *msgpack.Decoder
	tags map[string]string
	name string
}

type dpRecord struct {
	t    time.Time
	data interface{}
}

// NewDpSource returns a new DpSource which reads from an underlying io.Reader
func NewDpSource(rdr io.Reader, name string, tags map[string]string) *DpSource {
	s := &DpSource{
		rdr:  rdr,
		tags: tags,
		name: name,
		vals: make([]interface{}, 3),
		dec:  msgpack.NewDecoder(rdr),
	}
	return s
}

// Decode a MessagePack encoded dpRecord
func decodeRecord(dec *msgpack.Decoder) (dpRecord, error) {
	var (
		secs, usecs int64
		rec         dpRecord
	)

	n, err := dec.DecodeArrayLen()
	if err != nil {
		return rec, err
	}
	if n != 3 {
		return rec, fmt.Errorf("Bad array length: %d", n)
	}

	secs, err = dec.DecodeInt64()
	if err != nil {
		return rec, errors.New("Cannot decode seconds field")
	}

	usecs, err = dec.DecodeInt64()
	if err != nil {
		return rec, errors.New("Cannot decode microseconds field")
	}

	rec.t = time.Unix(secs, usecs*1000)
	rec.data, err = dec.DecodeInterface()

	return rec, err
}

func (s *DpSource) Next() bool {
	var rec dpRecord

	rec, s.err = decodeRecord(s.dec)
	if s.err != nil {
		return false
	}
	s.rec = make(map[string]interface{})

	switch d := rec.data.(type) {
	case map[string]interface{}:
		s.rec = d
		s.t = rec.t
		// Profiler state is stored as a tag
		if state, ok := s.rec["state"]; ok {
			s.tags["state"] = state.(string)
			delete(s.rec, "state")
		}
	case string:
		ev := dputil.ParseEvent(d)
		s.t = time.Unix(ev.Attrs["t"].(int64), 0)
		delete(ev.Attrs, "t")
		s.tags["type"] = ev.Name
		s.rec = ev.Attrs
	case []byte:
		ev := dputil.ParseEvent(string(d))
		s.t = time.Unix(ev.Attrs["t"].(int64), 0)
		delete(ev.Attrs, "t")
		s.tags["type"] = ev.Name
		s.rec = ev.Attrs
	}

	return true
}

func (s *DpSource) Err() error {
	if s.err != io.EOF {
		return s.err
	}
	return nil
}

func (s *DpSource) Name() string {
	return s.name
}

func (s *DpSource) Tags() map[string]string {
	return s.tags
}

func (s *DpSource) Measurement() (time.Time, map[string]interface{}) {
	return s.t, s.rec
}

func (s *DpSource) Precision() string {
	return "us"
}

// Filter is used to filter DataSource measurements
type Filter func(map[string]interface{}) map[string]interface{}

func NullFilter() Filter {
	return func(m map[string]interface{}) map[string]interface{} {
		return m
	}
}

func scaleFloat64(v interface{}, mult float64) float64 {
	switch x := v.(type) {
	case int64:
		return float64(x) * mult
	case uint64:
		return float64(x) * mult
	case int32:
		return float64(x) * mult
	case uint32:
		return float64(x) * mult
	case int16:
		return float64(x) * mult
	case uint16:
		return float64(x) * mult
	case int:
		return float64(x) * mult
	case uint:
		return float64(x) * mult
	}

	return 0
}

// Filter applied to profiler data records
func ProfilerFilter(scaled map[string]float64) Filter {
	return func(m map[string]interface{}) map[string]interface{} {
		m2 := make(map[string]interface{})
		for k, v := range m {
			if x, ok := scaled[k]; ok {
				m2[k] = scaleFloat64(v, x)
			} else {
				m2[k] = v
			}
		}

		// Make sure the profile number is a signed integer
		switch x := m["profile"].(type) {
		case uint64:
			m2["profile"] = int64(x)
		default:
			m2["profile"] = m["profile"]
		}

		// Expand the list of internal temperature values
		for i, val := range m["itemp"].([]interface{}) {
			switch x := val.(type) {
			case int64:
				m2[fmt.Sprintf("itemp_%d", i)] = float64(x) * float64(0.1)
			case uint64:
				m2[fmt.Sprintf("itemp_%d", i)] = float64(x) * float64(0.1)
			case int:
				m2[fmt.Sprintf("itemp_%d", i)] = float64(x) * float64(0.1)
			case uint:
				m2[fmt.Sprintf("itemp_%d", i)] = float64(x) * float64(0.1)
			}

		}
		delete(m2, "itemp")

		return m2
	}
}

// FixUnsigned converts all uint64 values to int64 because the InfluxDB
// client will store uint64 values as strings.
func FixUnsigned() Filter {
	return func(m map[string]interface{}) map[string]interface{} {
		m2 := make(map[string]interface{})
		for k, v := range m {
			if x, ok := v.(uint64); ok {
				m2[k] = int64(x)
			} else {
				m2[k] = v
			}
		}
		return m2
	}
}

//  LocalWords:  pnum Profiler itemp
