// Read a directory of Deep Profiler data files and write the data
// records to an InfluxDB database.
package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"path"
	"runtime"
	"strings"
	"sync"
	"syscall"

	client "github.com/influxdata/influxdb1-client/v2"
	"golang.org/x/crypto/ssh/terminal"
	"golang.org/x/exp/slog"
)

var Version = "dev"
var BuildDate = "unknown"

var Usage = `
Usage: dpstore [options] DATADIR DBURL DBNAME

Read a series of DP data files from DATADIR and write the data
records to an InfluxDB database DBNAME at DBURL
`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dbUser    string
	tagList   string
	batchSize int = 1000
	debugMode bool
)

var scaleValues = map[string]float64{
	"pressure":      0.001,
	"voltage":       0.001,
	"current":       0.001,
	"energy":        0.001,
	"motor_current": 0.001,
	"vmon":          0.001,
	"humidity":      0.1,
	"rel_charge":    1.0,
	"speed":         0.001,
	"ipr":           0.1,
}

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func getPassword(prompt string) ([]byte, error) {
	fd := int(os.Stdin.Fd())
	state, err := terminal.GetState(fd)
	if err != nil {
		return nil, err
	}

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt, os.Kill)
	defer signal.Stop(ch)

	go func() {
		_, ok := <-ch
		if ok {
			terminal.Restore(fd, state)
			os.Exit(1)
		}
	}()

	fmt.Print(prompt)
	pass, err := terminal.ReadPassword(fd)
	fmt.Println("")

	return pass, err
}

func openClient(url string) (client.Client, error) {
	cfg := client.HTTPConfig{
		Addr: url,
	}

	if dbUser != "" {
		creds := strings.Split(dbUser, ":")
		cfg.Username = creds[0]
		switch len(creds) {
		case 1:
			pass, err := getPassword("Database password: ")
			if err != nil {
				abort("get password", err)
			}
			cfg.Password = string(pass)
		case 2:
			cfg.Password = creds[1]
		}
	}

	return client.NewHTTPClient(cfg)
}

func streamData(ctx context.Context, ds DataSource, f Filter, ch chan<- *client.Point) {
	slog.Info("start data stream", slog.String("src", ds.Name()))
	defer func() {
		slog.Info("stop data stream", slog.String("src", ds.Name()))
	}()

	for ds.Next() {
		select {
		case <-ctx.Done():
			slog.Info("read canceled", slog.String("src", ds.Name()))
			return
		default:
		}
		t, fields := ds.Measurement()
		pt, err := client.NewPoint(ds.Name(),
			ds.Tags(),
			f(fields), t)
		if err != nil {
			slog.Error("invalid data point", slog.Any("err", err),
				slog.String("src", ds.Name()),
				slog.String("timestamp", t.String()))
			return
		} else {
			ch <- pt
		}
	}

	if err := ds.Err(); err != nil {
		slog.Error("read error", slog.Any("err", err),
			slog.String("src", ds.Name()))
	}
}

func copyTags(tags map[string]string) map[string]string {
	m := make(map[string]string)
	for k, v := range tags {
		m[k] = v
	}
	return m
}

func launchReaders(ctx context.Context, srcs map[string]string,
	tags map[string]string) <-chan *client.Point {
	wg := sync.WaitGroup{}
	ch := make(chan *client.Point, len(srcs))
	files := make([]*os.File, 0)

	for name, path := range srcs {
		f, err := os.Open(path)
		if err != nil {
			slog.Error("file open", slog.Any("err", err),
				slog.String("path", path))
			continue
		}
		files = append(files, f)
		ds := NewDpSource(f, name, copyTags(tags))
		wg.Add(1)
		if name == "profiler" {
			go func() {
				defer wg.Done()
				streamData(ctx, ds, ProfilerFilter(scaleValues), ch)
			}()
		} else {
			go func() {
				defer wg.Done()
				streamData(ctx, ds, FixUnsigned(), ch)
			}()
		}
	}

	// Wait for all of the streamData goroutines to finish then close
	// the channel.
	go func() {
		wg.Wait()
		for _, f := range files {
			f.Close()
		}
		close(ch)
		slog.Info("data streams closed")
	}()

	return ch
}

// Read a profile data directory and return a map of sensor names
// to file names.
func readDir(dirname string) map[string]string {
	sources := map[string]string{}

	// Filename format: NAME_TIMESTAMP_PROFILENUM.mpk
	// NAME may contain embedded underscores.
	files, _ := ioutil.ReadDir(dirname)
	var name string

	for _, f := range files {
		parts := strings.Split(f.Name(), "_")
		if parts[0] == "sernums" {
			// Not a data file
			continue
		}

		if n := len(parts); n > 3 {
			name = strings.Join(parts[0:n-2], "_")
		} else {
			name = parts[0]
		}

		if name == "evlogger" {
			name = "events"
		}
		sources[name] = path.Join(dirname, f.Name())
	}
	return sources
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}
	flag.StringVar(&dbUser, "user", lookupEnvOrString("DP_DB_USER", dbUser),
		"Database username:password")
	flag.StringVar(&tagList, "tags", lookupEnvOrString("DP_DB_TAGS", tagList),
		"Tags (name=value) to add to every entry")
	flag.IntVar(&batchSize, "batch", batchSize, "Batch size for database submission")
	flag.BoolVar(&debugMode, "debug", false, "If true, print records to stdout")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func dumpBatch(bp client.BatchPoints) {
	for _, pt := range bp.Points() {
		fmt.Printf("%v\n", pt)
	}
}

func main() {
	args := parseCmdLine()
	if len(args) < 3 {
		flag.Usage()
		os.Exit(1)
	}

	logger := initLogger(false)
	slog.SetDefault(logger)

	cln, err := openClient(args[1])
	if err != nil {
		abort("create InfluxDB Client", err)
	}
	defer cln.Close()

	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  args[2],
		Precision: "us",
	})

	tags := make(map[string]string)
	if tagList != "" {
		for _, s := range strings.Split(tagList, ",") {
			entry := strings.SplitN(s, "=", 2)
			if len(entry) == 2 {
				tags[entry[0]] = entry[1]
			}
		}
	}

	var (
		ctx    context.Context
		cancel context.CancelFunc
	)

	ctx, cancel = context.WithCancel(context.Background())
	defer cancel()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	defer signal.Stop(sigs)

	go func() {
		s, more := <-sigs
		if more {
			slog.Info("interrupted", slog.String("signal", s.String()))
			cancel()
		}
	}()

	sources := readDir(args[0])
	ch := launchReaders(ctx, sources, tags)
	count := 0

	for pt := range ch {
		bp.AddPoint(pt)
		count++
		if count == batchSize {
			slog.Info("batch write", slog.Int("n", count))
			if debugMode {
				dumpBatch(bp)
			} else {
				err = cln.Write(bp)
			}
			if err != nil {
				slog.Error("db write", slog.Any("err", err))
			}
			count = 0
			bp, _ = client.NewBatchPoints(client.BatchPointsConfig{
				Database:  args[2],
				Precision: "us",
			})
		}
	}

	if count > 0 {
		slog.Info("batch write", slog.Int("n", count))
		if debugMode {
			dumpBatch(bp)
		} else {
			err = cln.Write(bp)
		}
		if err != nil {
			slog.Error("db write", slog.Any("err", err))
		}
	}

}
